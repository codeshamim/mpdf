<?php

// Require composer autoload
require_once __DIR__ . '/vendor/autoload.php';

$config = [
    'mode'                     => '-aCJK',
    "allowCJKoverflow"         => true,
    "autoScriptToLang"         => true,
   "allow_charset_conversion" => true,
   "autoLangToFont"           => true,
];
$mpdf = new \Mpdf\Mpdf($config);


$currencies = array(
                  'AED' => '&#x62f;.&#x625;',
                  'AFN' => '&#x60b;',
                  'ALL' => 'L',
                  'AMD' => 'AMD',
                  'ANG' => '&fnof;',
                  'AOA' => 'Kz',
                  'ARS' => '&#36;',
                  'AUD' => '&#36;',
                  'AWG' => 'Afl.',
                  'AZN' => 'AZN',
                  'BAM' => 'KM',
                  'BBD' => '&#36;',
                  'BDT' => '&#2547;&nbsp;',
                  'BGN' => '&#1083;&#1074;.',
                  'BHD' => '.&#x62f;.&#x628;',
                  'BIF' => 'Fr',
                  'BMD' => '&#36;',
                  'BND' => '&#36;',
                  'BOB' => 'Bs.',
                  'BRL' => '&#82;&#36;',
                  'BSD' => '&#36;',
                  'BTC' => '&#3647;',
                  'BTN' => 'Nu.',
                  'BWP' => 'P',
                  'BYR' => 'Br',
                  'BYN' => 'Br',
                  'BZD' => '&#36;',
                  'CAD' => '&#36;',
                  'CDF' => 'Fr',
                  'CHF' => '&#67;&#72;&#70;',
                  'CLP' => '&#36;',
                  'CNY' => '&yen;',
                  'COP' => '&#36;',
                  'CRC' => '&#x20a1;',
                  'CUC' => '&#36;',
                  'CUP' => '&#36;',
                  'CVE' => '&#36;',
                  'CZK' => '&#75;&#269;',
                  'DJF' => 'Fr',
                  'DKK' => 'DKK',
                  'DOP' => 'RD&#36;',
                  'DZD' => '&#x62f;.&#x62c;',
                  'EGP' => 'EGP',
                  'ERN' => 'Nfk',
                  'ETB' => 'Br',
                  'EUR' => '&euro;',
                  'FJD' => '&#36;',
                  'FKP' => '&pound;',
                  'GBP' => '&pound;',
                  'GEL' => '&#x20be;',
                  'GGP' => '&pound;',
                  'GHS' => '&#x20b5;',
                  'GIP' => '&pound;',
                  'GMD' => 'D',
                  'GNF' => 'Fr',
                  'GTQ' => 'Q',
                  'GYD' => '&#36;',
                  'HKD' => '&#36;',
                  'HNL' => 'L',
                  'HRK' => 'kn',
                  'HTG' => 'G',
                  'HUF' => '&#70;&#116;',
                  'IDR' => 'Rp',
                  'ILS' => '&#8362;',
                  'IMP' => '&pound;',
                  'INR' => '&#8377;',
                  'IQD' => '&#x639;.&#x62f;',
                  'IRR' => '&#xfdfc;',
                  'IRT' => '&#x062A;&#x0648;&#x0645;&#x0627;&#x0646;',
                  'ISK' => 'kr.',
                  'JEP' => '&pound;',
                  'JMD' => '&#36;',
                  'JOD' => '&#x62f;.&#x627;',
                  'JPY' => '&yen;',
                  'KES' => 'KSh',
                  'KGS' => '&#x441;&#x43e;&#x43c;',
                  'KHR' => '&#x17db;',
                  'KMF' => 'Fr',
                  'KPW' => '&#x20a9;',
                  'KRW' => '&#8361;',
                  'KWD' => '&#x62f;.&#x643;',
                  'KYD' => '&#36;',
                  'KZT' => '&#8376;',
                  'LAK' => '&#8365;',
                  'LBP' => '&#x644;.&#x644;',
                  'LKR' => '&#xdbb;&#xdd4;',
                  'LRD' => '&#36;',
                  'LSL' => 'L',
                  'LYD' => '&#x644;.&#x62f;',
                  'MAD' => '&#x62f;.&#x645;.',
                  'MDL' => 'MDL',
                  'MGA' => 'Ar',
                  'MKD' => '&#x434;&#x435;&#x43d;',
                  'MMK' => 'Ks',
                  'MNT' => '&#x20ae;',
                  'MOP' => 'P',
                  'MRU' => 'UM',
                  'MUR' => '&#x20a8;',
                  'MVR' => '.&#x783;',
                  'MWK' => 'MK',
                  'MXN' => '&#36;',
                  'MYR' => '&#82;&#77;',
                  'MZN' => 'MT',
                  'NAD' => 'N&#36;',
                  'NGN' => '&#8358;',
                  'NIO' => 'C&#36;',
                  'NOK' => '&#107;&#114;',
                  'NPR' => '&#8360;',
                  'NZD' => '&#36;',
                  'OMR' => '&#x631;.&#x639;.',
                  'PAB' => 'B/.',
                  'PEN' => 'S/',
                  'PGK' => 'K',
                  'PHP' => '&#8369;',
                  'PKR' => '&#8360;',
                  'PLN' => '&#122;&#322;',
                  'PRB' => '&#x440;.',
                  'PYG' => '&#8370;',
                  'QAR' => '&#x631;.&#x642;',
                  'RMB' => '&yen;',
                  'RON' => 'lei',
                  'RSD' => '&#1088;&#1089;&#1076;',
                  'RUB' => '&#8381;',
                  'RWF' => 'Fr',
                  'SAR' => '&#x631;.&#x633;',
                  'SBD' => '&#36;',
                  'SCR' => '&#x20a8;',
                  'SDG' => '&#x62c;.&#x633;.',
                  'SEK' => '&#107;&#114;',
                  'SGD' => '&#36;',
                  'SHP' => '&pound;',
                  'SLL' => 'Le',
                  'SOS' => 'Sh',
                  'SRD' => '&#36;',
                  'SSP' => '&pound;',
                  'STN' => 'Db',
                  'SYP' => '&#x644;.&#x633;',
                  'SZL' => 'L',
                  'THB' => '&#3647;',
                  'TJS' => '&#x405;&#x41c;',
                  'TMT' => 'm',
                  'TND' => '&#x62f;.&#x62a;',
                  'TOP' => 'T&#36;',
                  'TRY' => '&#8378;',
                  'TTD' => '&#36;',
                  'TWD' => '&#78;&#84;&#36;',
                  'TZS' => 'Sh',
                  'UAH' => '&#8372;',
                  'UGX' => 'UGX',
                  'USD' => '&#36;',
                  'UYU' => '&#36;',
                  'UZS' => 'UZS',
                  'VEF' => 'Bs F',
                  'VES' => 'Bs.S',
                  'VND' => '&#8363;',
                  'VUV' => 'Vt',
                  'WST' => 'T',
                  'XAF' => 'CFA',
                  'XCD' => '&#36;',
                  'XOF' => 'CFA',
                  'XPF' => 'Fr',
                  'YER' => '&#xfdfc;',
                  'ZAR' => '&#82;',
                  'ZMW' => 'ZK',
            ); 

$data="";
foreach ($currencies as $key => $value) {
      $data.="<tr>";
      $data.="      <td></td>";
      $data.="      <td>$key</td>";
      $data.="      <td>$value</td>";
      $data.="      <td>Ok</name>";
      $data.="</tr>";
     
}



$html ="



<table border='1' cellpadding='20'>
   <tr>
      <td>Sirial</td>
      <td>Language Name</td>
      <td>Language Text</td>
      <td>Status</td>
   </tr>


$data


<tr>
      <td>1</td>
      <td>Afrikaans</td>
      <td>Bly tuis, bly veilig</td>
      <td>Ok</name>
</tr>




<tr>
      <td>2</td>
      <td>Albanian</td>
      <td>
       
Qëndro në shtëpi, qëndro i sigurt
      </td>
      <td>Ok</td>
</tr>


<tr>
      <td>3</td>
      <td>Amharic</td>
      <td>
        ቤትዎ ይቆዩ ፣ ደህንነትዎ የተጠበቀ ይሁን
      </td>
      <td>Ok</td>
</tr>





<tr>
      <td>4</td>
      <td>Arabic</td>
      <td>البقاء في المنزل ، والبقاء في أمان</td>
      <td>Ok</td>
</tr>







<tr>
      <td>5</td>
      <td>Armenian</td>
      <td>
Մնացեք տուն, ապահով եղեք
</td>
      <td>Ok</td>
</tr>





<tr>
      <td>6</td>
      <td>Azerbaijani</td>
      <td>
Evdə qalın, etibarlı olun
</td>
      <td>Ok</td>
</tr>




<tr>
      <td>7</td>
      <td>Bangla</td>
      <td>

বাড়িতে থাকুন, নিরাপদে থাকুন
</td>
      <td>Ok</td>
</tr>



<tr>
      <td>8</td>
      <td>Basque</td>
      <td>
Egon etxean, egon seguru
</td>
      <td>Ok</td>
</tr>




<tr>
      <td>9</td>
      <td>Belarusian</td>
      <td> Будзь дома, заставайся ў бяспецы </td>
      <td>Ok</td>
</tr>


<tr>
      <td>10</td>
      <td>Bosnian</td>
      <td> Ostanite kod kuće, budite sigurni </td>
      <td>Ok</td>
</tr>


<tr>
      <td>11</td>
      <td>Bulgarian</td>
      <td> Останете вкъщи, останете в безопасност </td>
      <td>Ok</td>
</tr>






<tr>
      <td>12</td>
      <td>Burmese</td>
      <td> နေပါ၊ လုံခြုံပါ </td>
      <td>Ok</td>
</tr>


<tr>
      <td>13</td>
      <td>Catalan</td>
      <td> Allotgeu-vos a casa vostra, estigueu segurs </td>
      <td>Ok</td>
</tr>

<tr>
      <td>14</td>
      <td>Cebuano</td>
      <td> Pagpabilin sa Panimalay, Magpabilin nga Luwas </td>
      <td>Ok</td>
</tr>



<tr>
      <td>15</td>
      <td>Chinese (Simplified)</td>
      <td> 待在家里，安全 </td>
      <td>Ok</td>
</tr>



<tr>
      <td>16</td>
      <td>Chinese (Traditional)</td>
      <td> 待在家裡，安全 </td>
      <td>Ok</td>
</tr>


<tr>
      <td>17</td>
      <td>Corsican</td>
      <td> Fighjate à a casa, stà sicuru </td>
      <td>Ok</td>
</tr>


<tr>
      <td>18</td>
      <td>Croatian</td>
      <td> Ostanite kod kuće, budite sigurni </td>
      <td>Ok</td>
</tr>




<tr>
      <td>19</td>
      <td>Czech</td>
      <td> Zůstaňte doma, zůstaňte v bezpečí </td>
      <td>Ok</td>
</tr>



<tr>
      <td>20</td>
      <td>Danish</td>
      <td> Bliv hjemme, vær sikker </td>
      <td>Ok</td>
</tr>


<tr>
      <td>21</td>
      <td>Dutch</td>
      <td> Blijf thuis, blijf veilig </td>
      <td>Ok</td>
</tr>

<tr>
      <td>22</td>
      <td>English</td>
      <td> Stay Home, Stay Safe </td>
      <td>Ok</td>
</tr>


<tr>
      <td>23</td>
      <td>Esperanto </td>
      <td> Restu hejme, restu sekura </td>
      <td>Ok</td>
</tr>

<tr>
      <td>24</td>
      <td>Estonian </td>
      <td> Jää koju, ole turvaline </td>
      <td>Ok</td>
</tr>


<tr>
      <td>25</td>
      <td> Filipino </td>
      <td> Manatili sa Bahay, Manatiling Ligtas </td>
      <td>Ok</td>
</tr>


<tr>
      <td>26</td>
      <td> Finnish </td>
      <td> Pysy kotona, pysy turvassa </td>
      <td>Ok</td>
</tr>






<tr>
      <td>27</td>
      <td> French </td>
      <td> Restez à la maison, restez en sécurité </td>
      <td>Ok</td>
</tr>


<tr>
      <td>28</td>
      <td> Georgian </td>
      <td> დარჩი სახლში, იყავი უსაფრთხო </td>
      <td>Ok</td>
</tr>


<tr>
      <td>29</td>
      <td> German </td>
      <td> Bleib zu Hause, bleib sicher </td>
      <td>Ok</td>
</tr>




<tr>
      <td>30</td>
      <td> Greek </td>
      <td> Μείνετε σπίτι, μείνετε ασφαλείς </td>
      <td>Ok</td>
</tr>


<tr>
      <td>31</td>
      <td> Gujarati </td>
      <td> ઘરે રહો, સલામત રહો </td>
      <td>Ok</td>
</tr>

<tr>
      <td>32</td>
      <td> Haitian Creole </td>
      <td> Rete lakay ou, rete an sekirite </td>
      <td>Ok</td>
</tr>

<tr>
      <td>33</td>
      <td> Hausa </td>
      <td> Kasance a Gida, A zauna lafiya </td>
      <td>Ok</td>
</tr>


<tr>
      <td>34</td>
      <td> Hawaiian </td>
      <td> Noho home, noho palekana </td>
      <td>Ok</td>
</tr>


<tr>
      <td>35</td>
      <td> Hebrew </td>
      <td> הישאר בבית, הישאר בטוח </td>
      <td>Ok</td>
</tr>



<tr>
      <td>36</td>
      <td> Hindi </td>
      <td> घर रहें, सुरक्षित रहें </td>
      <td>Ok</td>
</tr>




<tr>
      <td>37</td>
      <td> Hmong </td>
      <td> Nyob Hauv Tsev, Nyob Zoo </td>
      <td>Ok</td>
</tr>



<tr>
      <td>38</td>
      <td> Hungarian </td>
      <td> Maradjon otthon, maradjon biztonságban </td>
      <td>Ok</td>
</tr>


<tr>
      <td>39</td>
      <td> Icelandic </td>
      <td> Vertu heima, vertu öruggur </td>
      <td>Ok</td>
</tr>


<tr>
      <td>40</td>
      <td> Igbo </td>
      <td> Nọrọ nụlọ, nọrọ ọdụ </td>
      <td>Ok</td>
</tr>



<tr>
      <td>41</td>
      <td> Indonesian </td>
      <td> Tetap di Rumah, Tetap Aman </td>
      <td>Ok</td>
</tr>


<tr>
      <td>42</td>
      <td> Irish </td>
      <td> Fan sa Bhaile, Fan Sábháilte </td>
      <td>Ok</td>
</tr>




<tr>
      <td>43</td>
      <td> Italian </td>
      <td> Stai a casa, stai al sicuro </td>
      <td>Ok</td>
</tr>


<tr>
      <td>44</td>
      <td> Japanese </td>
      <td> 家にいて安全に </td>
      <td>Ok</td>
</tr>

<tr>
      <td>45</td>
      <td> Javanese </td>
      <td> Tetep Ngarep, Tetep Aman </td>
      <td>Ok</td>
</tr>



<tr>
      <td>46</td>
      <td> Kannada </td>
      <td> ಮನೆಯಲ್ಲೇ ಇರಿ, ಸುರಕ್ಷಿತವಾಗಿರಿ </td>
      <td>Ok</td>
</tr>



<tr>
      <td>47</td>
      <td> Kazakh </td>
      <td> Үйде бол, қауіпсіз бол </td>
      <td>Ok</td>
</tr>



<tr>
      <td>48</td>
      <td> Khmer </td>
      <td> ស្នាក់នៅផ្ទះមានសុវត្ថិភាព </td>
      <td>Ok</td>
</tr>




<tr>
      <td>49</td>
      <td> Kinyarwanda </td>
      <td> Guma murugo, Gumana umutekano </td>
      <td>Ok</td>
</tr>




<tr>
      <td>50</td>
      <td> Korean </td>
      <td> 집에있어, 안전 해 </td>
      <td>Ok</td>
</tr>



<tr>
      <td>51</td>
      <td> Kurdish </td>
      <td> Li malê bimîne, ewle bimîne </td>
      <td>Ok</td>
</tr>


<tr>
      <td>52</td>
      <td> Kyrgyz </td>
      <td> Үйдө кал, Коопсуз бол </td>
      <td>Ok</td>
</tr>


<tr>
      <td>53</td>
      <td> Lao </td>
      <td> ຢູ່ເຮືອນ, ຢູ່ຢ່າງປອດໄພ </td>
      <td>Ok</td>
</tr>

<tr>
      <td>54</td>
      <td> Latin </td>
      <td> Sede in domo tua: manete tutum </td>
      <td>Ok</td>
</tr>


<tr>
      <td>55</td>
      <td> Latvian </td>
      <td> Palieciet mājās, esiet droši </td>
      <td>Ok</td>
</tr>



<tr>
      <td>56</td>
      <td> Lithuanian </td>
      <td> Likite namuose, saugiai </td>
      <td>Ok</td>
</tr>



<tr>
      <td>57</td>
      <td> Luxembourgish </td>
      <td> Bleift doheem, bleift sécher </td>
      <td>Ok</td>
</tr>


<tr>
      <td>58</td>
      <td> Macedonian </td>
      <td> Останете дома, бидете безбедни </td>
      <td>Ok</td>
</tr>



<tr>
      <td>59</td>
      <td> Malagasy </td>
      <td> Mijanòna any an-trano, mijanòna soa aman-tsara </td>
      <td>Ok</td>
</tr>


<tr>
      <td>60</td>
      <td> Malayalam </td>
      <td> വീട്ടിൽ തന്നെ തുടരുക, സുരക്ഷിതമായി തുടരുക </td>
      <td>Ok</td>
</tr>





<tr>
      <td>61</td>
      <td> Maltese </td>
      <td> Ibqa d-dar, ibqa sikur </td>
      <td>Ok</td>
</tr>




<tr>
      <td>62</td>
      <td> Maori </td>
      <td> Kia noho ki te kainga, kia noho humarie </td>
      <td>Ok</td>
</tr>



<tr>
      <td>63</td>
      <td> Marathi </td>
      <td>घरी रहा, सुरक्षित रहा </td>
      <td>Ok</td>
</tr>




<tr>
      <td>64</td>
      <td> Mongolian </td>
      <td> Гэртээ бай, аюулгүй бай </td>
      <td>Ok</td>
</tr>




<tr>
      <td>65</td>
      <td> Nepali </td>
      <td> घर रहनुहोस्, सुरक्षित रहनुहोस् </td>
      <td>Ok</td>
</tr>




<tr>
      <td>66</td>
      <td> Norwegian </td>
      <td> Hold deg hjemme, vær trygg </td>
      <td>Ok</td>
</tr>



<tr>
      <td>67</td>
      <td> Nyanja </td>
      <td> Khalani Panyumba, Khalani Otetezeka </td>
      <td>Ok</td>
</tr>



<tr>
      <td>68</td>
      <td> Odia </td>
      <td> ଘରେ ରୁହ, ସୁରକ୍ଷିତ ରୁହ | </td>
      <td>Ok</td>
</tr>

<tr>
      <td>69</td>
      <td> Pashto </td>
      <td> کور کې پاتې شئ ، خوندي اوسئ  </td>
      <td>Ok</td>
</tr>



<tr>
      <td>70</td>
      <td> Persian </td>
      <td> در خانه بمانید ، ایمن باشید </td>
      <td>Ok</td>
</tr>


<tr>
      <td>71</td>
      <td> Portuguese </td>
      <td> Fique em casa, fique seguro </d>
      <td>Ok</td>
</tr>


<tr>
      <td>72</td>
      <td> Punjabi </td>
      <td> ਘਰ ਰਹੋ, ਸੁਰੱਖਿਅਤ ਰਹੋ </d>
      <td>Ok</td>
</tr>


<tr>
      <td>73</td>
      <td> Romanian </td>
      <td> Rămâi acasă, stai în siguranță </d>
      <td>Ok</td>
</tr>

<tr>
      <td>74</td>
      <td> Russian </td>
      <td> Оставайся дома, оставайся в безопасности </d>
      <td>Ok</td>
</tr>

<tr>
      <td>75</td>
      <td> Samoan </td>
      <td> Tumau i le Aiga, Nofoaga Saogalemu </d>
      <td>Ok</td>
</tr>



<tr>
      <td>76</td>
      <td> Scottish Gaelic </td>
      <td> Fuirich dhachaigh, Fuirich sàbhailte </d>
      <td>Ok</td>
</tr>




<tr>
      <td>77</td>
      <td> Serbian </td>
      <td> Останите код куће, будите сигурни </d>
      <td>Ok</td>
</tr>

<tr>
      <td>78</td>
      <td> Shona </td>
      <td> Gara Imba, Gara Wakachengeteka </d>
      <td>Ok</td>
</tr>





<tr>
      <td>79</td>
      <td> Sindhi </td>
      <td> گهرو رهڻ ، محفوظ رهڻ  </d>
      <td>Ok</td>
</tr>




<tr>
      <td>80</td>
      <td> Sinhala </td>
      <td> ගෙදර ඉන්න, ආරක්ෂිතව ඉන්න   </d>
      <td>Ok</td>
</tr>


<tr>
      <td>81</td>
      <td> Slovak </td>
      <td> Zostaňte doma, zostaňte v bezpeč   </d>
      <td>Ok</td>
</tr>


<tr>
      <td>82</td>
      <td> Slovenian </td>
      <td> Ostani doma, ostani na varnem  </d>
      <td>Ok</td>
</tr>

<tr>
      <td>83</td>
      <td> Somali </td>
      <td> Guriga joog, Badbaadi   </d>
      <td>Ok</td>
</tr>



<tr>
      <td>84</td>
      <td> Southern Sotho </td>
      <td> Lula hae, U bolokehe   </d>
      <td>Ok</td>
</tr>



<tr>
      <td>85</td>
      <td> Spanish </td>
      <td> Quédese en casa, manténgase seguro   </d>
      <td>Ok</td>
</tr>




<tr>
      <td>86</td>
      <td> Sundanese </td>
      <td> Tetep di bumi, tetep aman   </d>
      <td>Ok</td>
</tr>





<tr>
      <td>87</td>
      <td> Swahili </td>
      <td> Kaa Nyumbani, Kaa salamaKaa Nyumbani, Kaa salama   </d>
      <td>Ok</td>
</tr>




<tr>
      <td>88</td>
      <td> Swedish </td>
      <td> Håll dig hemma, håll dig säker   </d>
      <td>Ok</td>
</tr>





<tr>
      <td>89</td>
      <td> Tajik </td>
      <td> Дар хона монед, бехатар бошед   </d>
      <td>Ok</td>
</tr>




<tr>
      <td>90</td>
      <td> Tamil </td>
      <td> வீட்டிலேயே இருங்கள், பாதுகாப்பாக இருங்கள்   </d>
      <td>Ok</td>
</tr>




<tr>
      <td>91</td>
      <td> Tatar </td>
      <td> Өйдә калыгыз, куркынычсыз булыгыз   </d>
      <td>Ok</td>
</tr>




<tr>
      <td>92</td>
      <td> Telugu </td>
      <td> ఇంటి వద్దే ఉండండి, సురక్షితంగా ఉండండి  </d>
      <td>Ok</td>
</tr>





<tr>
      <td>93</td>
      <td> Thai </td>
      <td> อยู่บ้านปลอดภัย  </d>
      <td>Ok</td>
</tr>




<tr>
      <td>94</td>
      <td> Turkish </td>
      <td> Evde Kalın, Güvende Kalın  </d>
      <td>Ok</td>
</tr>




<tr>
      <td>95</td>
      <td> Turkmen </td>
      <td> Öýde boluň, howpsuz boluň  </d>
      <td>Ok</td>
</tr>


<tr>
      <td>96</td>
      <td> Ukrainia </td>
      <td> Будьте вдома, будьте в безпеці </d>
      <td>Ok</td>
</tr>

<tr>
      <td>97</td>
      <td> Urdu </td>
      <td> گھر رہیں ، سلامت رہیں  </d>
      <td>Ok</td>
</tr>


<tr>
      <td>98</td>
      <td> Uyghur </td>
      <td> ۆيدە تۇرۇڭ ، بىخەتەر تۇرۇڭ </d>
      <td>Ok</td>
</tr>



<tr>
      <td>99</td>
      <td> Uzbek </td>
      <td> Uyda boling, xavfsiz boling </d>
      <td>Ok</td>
</tr>



<tr>
      <td>100</td>
      <td> Vietnamese </td>
      <td> Ở nhà, an toàn </d>
      <td>Ok</td>
</tr>





<tr>
      <td>101</td>
      <td> Welsh </td>
      <td> Arhoswch Gartref, Cadwch yn Ddiogel </d>
      <td>Ok</td>
</tr>




<tr>
      <td>102</td>
      <td> Western Frisian </td>
      <td> Bliuw thús, bliuw feilich </d>
      <td>Ok</td>
</tr>




<tr>
      <td>103</td>
      <td> Xhosa </td>
      <td> Hlala ekhaya, uhlale ukhuselekile </d>
      <td>Ok</td>
</tr>



<tr>
      <td>104</td>
      <td> Yiddish </td>
      <td> בלייַבן היים, בלייַבן זיכער  </d>
      <td>Ok</td>
</tr>




<tr>
      <td>105</td>
      <td> Yoruba </td>
      <td> Duro Ile, Duro Ailewu </d>
      <td>Ok</td>
</tr>



<tr>
      <td>106</td>
      <td> Zulu </td>
      <td> Hlala Ekhaya, Uphephe </d>
      <td>Ok</td>
</tr>








</table>



";

$mpdf->autoScriptToLang = true;
$mpdf->baseScript       = 1;
$mpdf->autoVietnamese   = true;
$mpdf->autoArabic       = true;

$mpdf->WriteHTML($html);
$mpdf->Output();
